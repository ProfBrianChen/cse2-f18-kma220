///Kevin Andrefsky
//CSE 002: Section 210, Arrays, lab08
//November 7th, 2018 
//This lab session will get you familiar with one-dimensional arrays.
import java.util.Random;
public class Arrays{
  public static void main (String [] args){
  int [] arrayOne = new int [100]; //first array
  Random rand = new Random();
  int [] arrayTwo = new int [100];//second array
  
  
System.out.println("Array 1 holds the following integers: ");//print statement 
  for(int a = 0; a<=99; a++){
    arrayOne[a] = (int)(Math.random()*100);
    System.out.println(arrayOne[a] + " ");
}



  for(int index = 0; index < arrayOne.length; index++){
    arrayTwo[arrayOne[index]]++;
}
System.out.println();

for(int b=0; b<=99; b++){ //Loop to print the amount of times it is counted 
if(arrayTwo[b] == 1)
  System.out.println(b + " occurs " + arrayTwo[b] + " time(s)"); //Print statement 
  else 
  System.out.println(b + " occurs " + arrayTwo[b] + " time(s)"); //Print statement 
}


  }
    
}