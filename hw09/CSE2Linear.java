//Kevin Andrefsky
//CSE 002: Section 10, hw09 - Fun with Searching
//November 27th, 2018
//This homework gives you practice with arrays and in searching single dimensional arrays
import java.util.Scanner;//importing the Scanner 
import java.util.Arrays;//Importing the .length  
public class CSE2Linear{
    public static void main (String[]args){ //main method 
        System.out.println("Enter 15 integers that are ascending for final grades in CSE2: "); //print statement asking for 15 integers 
        Scanner input = new Scanner(System.in);
        int count = 0; //intitializing the count
        int[] array1 = new int[15]; //relatingt the two arrays 
     while (count < 15){ //while loop for count when it is less than 15 
     if (input.hasNextInt()){ //if statement analyzing the next int 
         array1[count] = input.nextInt();
     if (array1[count] < 0 || array1[count] > 100){ //if statement with an or statement for the arrays 
         System.out.println("ERROR, please enter an integer within the range 0-100: "); //print statement for errors 
         System.exit(0);
   }
      else{
         if (count > 0){ //if statement for when count is greater than 0  
         if (array1[count] < array1[count - 1]){
          System.out.println("ERROR, please enter an integer greater than the last one: "); //print statement for errors   
          System.exit(0);
      }
  }
      count++; //incrementing count
      }
  }
    else{
     System.out.println("ERROR, please enter an integer: "); //Error message
     System.exit(0);
    }
 }
      System.out.println("Sorted: ");//print sorted
        System.out.println(Arrays.toString(array1));//print the Array
      System.out.print("Enter a grade to search for: ");
   if (input.hasNextInt()){ //if statement scanning the next int 
       int x = input.nextInt(); //intializing x 
       BinarySearch(array1, x);//Binary search 
       Scramble(array1);//scramble the array 
         System.out.println("Scrambled:"); //print scrambled 
         System.out.println(Arrays.toString(array1)); //print the Array 
         System.out.print("Enter a grade to search for: "); //print statement 
   if (input.hasNextInt()){
         LinearSearch(array1,x); //Linear Search 
  }
   else{
      System.out.println("ERROR, please enter an integer: "); //Error message 
      System.exit(0);
  }
}
   else{
      System.out.println("ERROR, please enter a grade to seach for: "); //Error message 
      System.exit(0);
  }
   }
public static void BinarySearch(int[] array2, int integer){ //Method for the binary search 
       int highIndex = array2.length - 1;//initializing the integers 
       int lowIndex = 0;
       int count = 0;
   while (lowIndex <= highIndex){//while loop for the low and high index 
       count++;
     int midIndex = (highIndex + lowIndex)/2;
   if (integer < array2[midIndex]){ //if statement if the integer is less than the mid of the index 
        highIndex = midIndex - 1; //the high index is the mid -1
   }
  else if (integer > array2[midIndex]){ //else if for the low index 
      lowIndex = midIndex + 1;
  }
    else if (integer == array2[midIndex]){
      System.out.println(integer + " was found in " + count + " iterations"); //print statement if the integer is equal to the array 2 mid index 
      break;
    }
   }
    if (lowIndex > highIndex){
  System.out.println(integer + " was not found in " + count + " iterations"); //print statement if the low index is less than the high index 
}
    }

public static void LinearSearch(int[] array1, int x){ //method that uses a linear search
   for (int a = 0; a < array1.length; a++){ //for loop to loop though the array 
        if (array1[a] == x){
     System.out.println(x + " was found in the list in " + (a + 1) + " iterations"); //print statement to print the iterations
                break;
 }
  else if (array1[a] != x && a == (array1.length - 1)){ //else if to compare the array to the array length 
       System.out.println(x + " was not found in " + (a + 1) + " iterations"); //print statement for the amount of iterations 
  }
 }
    }
public static int[] Scramble(int[] array1){ //method to scramble the array 
     for (int a = 0; a < array1.length; a++){ //for loop to loop through the array 
         int x = (int)(Math.random()*array1.length); //generating the random number 
         int storeValue = array1[a]; //initializing the store variable 
     while (x != a){ //while loop if x is not equal to a
         array1[a] = array1[x]; //swaping the variables
         array1[x] = storeValue; //setting the array equal to the store value 
           break;
    }
   }
    return array1;
}
}