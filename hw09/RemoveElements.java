//Kevin Andrefsky
//CSE 002: Section 10, hw09 - Fun with Searching 
//November 27th, 2018
//This homework gives you practice with arrays and in searching single dimensional arrays
import java.util.Scanner;//importing scanner 
import java.util.Random;//importing the random number generator 

public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
	  String answer="";
	do{
  	  System.out.print("Random input 10 ints [0-9]");
  	  num = randomInput();
  	    String out = "The original array is:";
  	  out += listArray(num);
  	  System.out.println(out);
 
  	System.out.print("Enter the index ");
  	    index = scan.nextInt();
  	    newArray1 = delete(num,index);
  	  String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
 public static int[] randomInput() { //Method to generate a random input 
  int array1[] = new int[10]; //declaring and initializing an array 
  Random random = new Random(); //importing random generator 
  for(int a = 0; a<array1.length; a++) { //For loop to generate a random array
      array1[a] = random.nextInt(9); //Random number is generated 
 }
   return array1; //return the array 
  }

 public static int[] delete(int[] list, int pos) { //Method to compare the list array and pos
   int array1[] = new int[list.length - 1]; //intitializing and declaring the array 
   int index = 0; //initializing the index 
   for(int a = 0; a < array1.length; a++) { //for loop to loop through the array 
      if (a == pos) { //if statement if a is equal to the pos
              index++; //incrementing index 
 }
    array1[a] = list[index]; //relating the array to the list array 
        index++; //incrementing the index 
 }
   return array1; //return the array 
}
 public static int[] remove(int[] list, int target){ //method to compare and relate the list array and the target int 
  int count = 0; //intitializing count 
  for (int a = 0; a<list.length; a++) //for loop to through the array at a 
     if (list[a] == target) { //if statement if the list array is equal to target 
         count++; //incrementing count 
 }
  int array1[] = new int[list.length-count]; //intitializing and declaring the array 
  int index = 0;
  for(int a = 0; a< array1.length; a++) { //for loop to loop through the array a
     if (list[index] == target) {
          index++;
 }
    array1[a] = list[index]; //comparing the array to the list array 
    index++;
}
    return array1; //returning the array 
  }
}
