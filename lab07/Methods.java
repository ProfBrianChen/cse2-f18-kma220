///Kevin Andrefsky
//CSE 002: Section 210, Methods, lab07
//October 24th, 2018
//Methods allow software developers to modularize and reuse their code in many contexts, reducing the chance for bugs and increasing productivity.  
//This lab will give you basic experience in declaring and calling methods. 
import java.util.Scanner;

public class Methods{
  
  public static String adjective(){
 
    int int1 = 0;
    String int12 = "0";
    int1 = (int) (Math.random()*10);
    switch (int1){
      case 0:
        int12 = "big";
        break;
        
        case 1:
        int12 = "small"; //Similar for all cases  
        break;
        
        case 2:
        int12 = "large";
        break;
        
        case 3:
        int12 = "tiny";
        break;
        
        case 4:
        int12 = "great";
        break;
        
        case 5:
        int12 = "bad";
        break;
        
        case 6: 
        int12 = "different";
        break;
        
        case 7:
        int12 = "high";
        break;
        
        case 8: 
        int12 = "low";  
        break;
        
        case 9:
        int12 = "ugly";
        break;
    
    }
    return int12;
  }
  
  public static String subjectnoun(){
    int int2 = 0;
    String int22 = "0";
    int2 = (int) (Math.random()*10);
    switch (int2){
        
      case 0:
        int22 = "dog";
        break; 
        
        case 1:
        int22 = "cat"; //Similar for all cases  
        break;
        
        case 2:
        int22 = "fox";
        break;
        
        case 3:
        int22 = "tiger";
        break;
        
        case 4:
        int22 = "lion";
        break;
        
        case 5:
        int22 = "cheetah";
        break;
        
        case 6: 
        int22 = "zebra";
        break;
        
        case 7:
        int22 = "goat";
        break;
        
        case 8: 
        int22 = "aligator";  
        break;
        
        case 9:
        int22 = "snake";
        break;
    }
    return int22;
  }
  
  
  public static String verb(){
    int int3 = 0;
    String int32 = "0";
    int3 = (int) (Math.random()*10);
    switch (int3){
    
        case 0:
        int32 = "ate";
        break;
        
        case 1:
        int32 = "chased"; //Similar for all cases  
        break;
        
        case 2:
        int32 = "liked";
        break;
        
        case 3:
        int32 = "ran";
        break;
        
        case 4:
        int32 = "placed";
        break;
        
        case 5:
        int32 = "followed";
        break;
        
        case 6: 
        int32 = "studied";
        break;
        
        case 7:
        int32 = "took";
        break;
        
        case 8: 
        int32 = "fell";  
        break;
        
        case 9:
        int32 = "walked";
        break;
    }
    return int32;
  } 
  
  
  public static String objectnoun(){
    int int4 = 0;
    String int42 = "0";
    int4 = (int) (Math.random()*10);
    switch (int4){
        
        case 0:
        int42 = "man";
        break;
        
        case 1:
        int42 = "woman"; //Similar for all cases  
        break;
        
        case 2:
        int42 = "cat";
        break;
        
        case 3:
        int42 = "cougar";
        break;
        
        case 4:
        int42 = "fish";
        break;
        
        case 5:
        int42 = "pen";
        break;
        
        case 6: 
        int42 = "chair";
        break;
        
        case 7:
        int42 = "bottle";
        break;
        
        case 8: 
        int42 = "bracelet";  
        break;
        
        case 9:
        int42 = "book";
        break;
    }
    return int42;
  }
  
  
  public static void main (String[] args){ //main method used to print out the sentence 
    boolean cont = true;
    int sequences = (int)(Math.random()*10);
    
    Scanner myScanner = new Scanner (System.in);
    
    String subject = subjectnoun();
   
      System.out.println("The " + adjective() + " " + adjective() + " " + subject + " " + verb() + " " + "the" + " " + adjective() + " " + objectnoun() + ".");
    
    
    do{
      int i = 1;
      for (i=1; i<= sequences; ++i){
        System.out.println("This " + subject + " " + verb() + " to the " + adjective() + " " + objectnoun() + ".");
      }
      
      
      System.out.println("That " + subject + " " + verb() + " her " + objectnoun() + ".");
      
  System.out.println("Would you like to print another story? (yes or no)");
      cont = myScanner.nextLine().equals("yes");
    }
      while (cont); //condition to continue the loop 
    
 
      
    
    }
    
}