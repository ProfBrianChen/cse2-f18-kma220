///Kevin Andrefsky
//CSE 002: Section 210
//October 23rd, 2018
//The objective of this homework is to practice using nested loops.
import java.util.Scanner; //Importing the scanner
//Main Method starts
public class EncryptedX{ 
 
  public static void main (String[] args){
    
    int n = 0; //declaring N as an int
    String junk = ""; //declaring a string for junk
    
    System.out.println("Please enter an integer between 1-100: "); //Prompting the user for an integer between 0-100
    Scanner myScanner = new Scanner (System.in); //scanning for the integer inputed
    while (myScanner.hasNextInt() == false) { //while loop to determine if the number entered is an integer
        System.out.println("Error in Value, Please input a valid integer. ");
        junk = myScanner.nextLine(); //Clearing the buffer
      }
   n = myScanner.nextInt(); //Assigning n to the integer inputed 
    junk = myScanner.nextLine(); //Clearing buffer
    
    if (n < 1 || n > 100){
      System.out.println("Error in Value, Please input a valid integer. "); //Print statement to prompt user to enter the correct integer 
      System.exit(0); //The program will exit if the input does not meet the requirements
      
    }
    for(int x=0; x<=n; ++x){ //For loop to print out the amount of iterations x goes through 
      for(int y=0; y<=n; ++y){ //Nested for loop printing the amount of iterations y goes through 
        if ((x==y) || (x + y==n)){ //If statement printing out spaces where necessary 
          System.out.print(" ");
        }
        else{ //Else statement printing an * where necessary
          System.out.print('*');
        }
      }
      System.out.println();
    }
    
    //End of main method
    
  }
  
}