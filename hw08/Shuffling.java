//Kevin Andrefsky
//CSE 002: Section 10, hw08 - Shuffling
//November 13th, 2018
//This homework gives you practice in manipulating arrays and in writing methods that have array parameters.
import java.util.Scanner;
import java.util.Random;
public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    //suit names
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //types of cards
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; //set number of cards to 5 
int again = 1; 
int index = 51; //index of 0-51
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; //The suits and numbers are divided
  System.out.print(cards[i]+" "); 
} 
System.out.println(); //Printing a space for code organization purposes 
  //calling methods
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ //If user wants another hand, the loop executes
   hand = getHand(cards,index,numCards); //calling method
   printArray(hand);//print statement
   index = (index) - (numCards);
   System.out.println("Enter a 1 if you want another hand drawn"); //Print statement if user would like another hand 
   again = scan.nextInt(); //reading the user's input 
}
}
public static void printArray(String[]list) { 
    for (int a = 0; a < list.length; a++) {//displaying the list 
            System.out.print(list[a] + " ");//printing the list 
    }
                System.out.println();//print statment for organization 
}
public static void shuffle(String[] list) {
    Random myrand = new Random();
    int index;  //initializing the index 
    String zero;
    for (int a = 0; a < 100; a++) {//for loop to shuffle cards
   index = myrand.nextInt(51) + 1;//shuffling statement 
   zero = list[0];
   list[0] = list[index];
   list[index] = zero;
    }
                      System.out.println();//print statement for organization 
}
public static String[] getHand(String[] list, int index, int numCards) { //Method to get the shuffled hand 
    String[] shuffledHand = {"","","","",""}; //
    for (int b = 0; b < numCards; b++) { //for loop to get the hand
                  shuffledHand[b] = list[index - b];
    } 
  return shuffledHand;  //returning the shuffled hand 
  
  
  
  
    }

}
  

