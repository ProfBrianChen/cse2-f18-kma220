///Kevin Andrefsky
//CSE 002: Section 210, Check lab03
//September 12th, 2018

import java.util.Scanner;

//This lab's objective is to take a check cost, and adding the tip to obtain a total cost.  That total cost is then distributed to each person who went to dinner. 


//Declaring the public class
public class Check{
  //main method required for every Java Program
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner( System.in ); //Declaring an instance of the Scanner and constructing it 
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //Printing the original cost of the check 
    double checkCost = myScanner.nextDouble(); // Declaring the double, the check cost for dinner 
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //Printing the percentage of the bill that is the tip
    double tipPercent = myScanner.nextDouble(); // declaring the double, the tip percentage
    tipPercent /= 100; //Converting the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //Printing the number of people who attended dinner
    int numPeople = myScanner.nextInt(); //Declaring the integer, the number of people who went to dinner
    
    double totalCost; //Declaring the double, the total cost of the meal with the tip
    double costPerPerson; //Declaring the double, the total cost per person
    int dollars, dimes, pennies;   //whole dollar, dimes, and pennies amount of the cost 
    totalCost = checkCost * (1 + tipPercent); //The total cost of the check with the tip included
    costPerPerson = totalCost / numPeople;  //The total cost divided by the number of people to obtain the total cost per person 
    
    dollars=(int)costPerPerson; //The amount of dollars each person owes
    dimes=(int)(costPerPerson * 10) % 10; //The amount of dimes each person owes 
    pennies=(int)(costPerPerson * 100) % 10; //The amount of pennies each person owes
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //Printing the amount of money each perosn who went to dinner owes 



    
  }//end of main method
  
}//end of class
