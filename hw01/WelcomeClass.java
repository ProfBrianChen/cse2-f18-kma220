///Kevin Andrefsky
//CSE 002: Section 210, Welcome Class Hw 01
//September 4th, 2018
public class WelcomeClass{
  
  public static void main(String args[]){
    //prints the program onto the terminal window
    System.out.println("      ----------- ");
    System.out.println("      | WELCOME | ");
    System.out.println("      ----------- ");
    System.out.println("   ^  ^  ^  ^  ^  ^  ");
    System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println(" <-K--M--A--2--2--0->");
    System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("   v  v  v  v  v  v  ");
    System.out.println("Hi my name is Kevin Andrefsky, I am from Ohio and I am a Junior majoring in Accounting and Finance. My favorite activities include lifting, running, and watching sports");
                       
  }
  
}