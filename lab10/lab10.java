///Kevin Andrefsky
//CSE 002: Section 210, lab10
//December 5th, 2018 
public class LAB10{
  public static void main (String [] args){
    int [][] list = {{1,2,7,3,6}
                     {9,4,6,2,1,0}};
  }
  public static void arraySort2DSelection(int [][] list){
    int [] list = {2,9,5,4,8,1,6};
    for (int i = 0; i < list.length - 1; i++){
      int currentMin =list[i];
      int currentMinIndex = i;
      
      for(int j = i +1; j < list.length; j++){
        if(currentMin > list[j]){
          currentMin = list[j];
          currentMinIndex = j;
        }
      }
      if(currentMinIndex != i){
        list[currentMinIndex] = list[i];
        list[i] = currentMin;
      }
    }
    for(int i = 0; i < list.length; i++)
      System.out.print(list[i] + " ");
  }
  public static void arraySort2DInsertion(int [][] list){
    int [] list = {2,9,5,4,8,1,6};
    for(int i = 1; i < list.length; i++){
      int currentElement = list[i];
      int k;
      for(k = i - 1; k >= 0  &&  list[k] > currentElement; k--){
        list[k +1] = list[k];
      }
      list[k + 1] = currentElement;
    }
     for(int i = 0; i < list.length; i++)
      System.out.print(list[i] + " ");
  }

}