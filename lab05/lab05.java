///Kevin Andrefsky
//CSE 002: Section 210, Card Generator lab04
//October 3rd, 2018
//This lab gives you practice with writing loops
import java.util.Scanner; //Importing the scanner 
public class lab05 {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in);
    
    //declaring integers and strings
    int courseNum = 0;
    String dept = "";
    int timesMet = 0;
    int startTime = 0;
    String instructorName = "";
    int numStudents = 0;
    String junk = "";
    
    System.out.println("What is the course number: ");//printing course number and scanning for the value 
      while (myScanner.hasNextInt() == false) {
        System.out.println("Error in Value, Please input an integer. ");
        junk = myScanner.nextLine();
      }
      
    courseNum = myScanner.nextInt(); 
    junk = myScanner.nextLine();
    
    System.out.println("Enter the department name: "); //enter the department name and scan the next line 
    
    dept = myScanner.nextLine();
    
    System.out.println("Enter the number of times the course met in a week: "); //print the number of times met and scan for the number while clearing the buffer
    while (myScanner.hasNextInt() == false) {
        System.out.println("Error in Value, Please input an integer. ");
        junk = myScanner.nextLine();
      }
    timesMet = myScanner.nextInt();
    
   System.out.println("Enter the start time of the class(military time): "); //Enter the start time in military time and scan for that integer 
    while (myScanner.hasNextInt() == false) {
        System.out.println("Error in Value, Please input an integer. ");
        junk = myScanner.nextLine();
      }
    startTime = myScanner.nextInt();
    junk = myScanner.nextLine();
    
    System.out.println("Enter the instructor's name: "); //enter the instructors name and scan for the name as a string 
    instructorName = myScanner.nextLine();
    
    System.out.println("Enter the number of students: "); //enter the number of students and scan for the number
    while (myScanner.hasNextInt() == false) {
        System.out.println("Error in Value, Please input an integer. ");
        junk = myScanner.nextLine();
      }
    numStudents = myScanner.nextInt();
    
    
   System.out.println("The course number is " + courseNum); //printing the final values of each objective
    System.out.println("The department name is " + dept);
    System.out.println("The number of times met in a week is " + timesMet);
    System.out.println("The instructors name is " + instructorName);
    System.out.println("The number of students is " + numStudents);
    
    
  }
}