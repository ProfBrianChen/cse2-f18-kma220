///Kevin Andrefsky
//CSE 002: Section 210
//October 9th, 2018
//The purpose of this homework is to get familiar with loops, a critical piece of syntax that is essential for many programming languages. The program you will write will print out a simple twist on the screen

import java.util.Scanner;//Importing the scanner
  public class TwistGenerator //defining the class 
  {
    //main  method required for every java program
    public static void main (String[] args)
    {
     //declaring strings and ints
      String top = "";
      String mid = "";
      String bot = "";
      int mod = 1;
      int input = 25;
      Scanner myScanner = new Scanner (System.in);
      System.out.println("Please enter an integer(1-25): "); //prompting an integer
        input = myScanner.nextInt(); //assigning the input to the integer printed 
      
       for (int i = 0; i < input; i++){ //a for loop which sets the parameters for the if statements 
         if (mod == 1){ //if statement if the mod is equal to 1
           top = top.concat("\\"); //what the top row is equal to 
           mid = mid.concat(" "); //what the middle row is equal to 
           bot = bot.concat("/"); //what the bottom row is equal to 
         } 
         if (mod == 2){ //if statement if the mod is equal to 2
           top = top.concat(" "); //what the top row is equal to 
           mid = mid.concat("X"); //what the middle row is equal to 
           bot = bot.concat(" "); //what the bottom row is equal to 
         }
         if (mod == 3){ //if statement if the mod is equal to 3
           top = top.concat("/"); //what the top row is equal to 
           mid = mid.concat(" "); //what the middle row is equal to 
           bot = bot.concat("\\"); //what the bottom row is equal to 
         }
          mod++; //the mod is incremented 
         if (mod == 4) //if statement if mod is equal to 4
           mod = 1; //mod is equal to 1
         
         }
      System.out.println(top); //print statements to print the generted twist (top row)
      System.out.println(mid); //print statement to print the generted twist (middle row)
      System.out.println(bot); //print statement to print the generted twist (bottome row)
 
    }
    
  }