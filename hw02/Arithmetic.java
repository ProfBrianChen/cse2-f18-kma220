///Kevin Andrefsky
//CSE 002: Section 210, ArithmeticCalculations hw02
//September 11th, 2018

import java.lang.Math;


  public class Arithmetic 
{

    public static void main(String[] args)
{
    //This program will manipulate data by running calculations and printing numerical outputs
    //Declare the integer of the number of pairs of pants as 3
    int numPants = 3;
    //Declaring the double, cost of a pair of pants as 34.98
    double pantsPrice = 34.98;
    //Declaring the integer, the number of sweatshirts as 2 
    int numShirts = 2;
    //Declaring the double, the cost per shirt, as 24.99
    double shirtPrice = 24.99;
    //Declaring the integer, the number of belts, as 1
    int numBelts = 1;
    //Declaring the double, the cost per belt, as 33.99
    double beltCost = 33.99;
    //Declaring the tax rate, as 0.06
    double paSalesTax = 0.06;
    
    
    //Declaring the double, the total cost of the pants
    double totalCostOfPants;
    //Declaring the double, the total cost of the shirts
    double totalCostOfShirts;
    //Declaring the double, the total cost of the belts
    double totalCostOfBelts;
    
    
    //Declaring the double, the sales tax charged on belts
    double salesTaxChargedOnBelts;
    //Declaring the double, the sales tax charged on shirts   
    double salesTaxChargedOnShirts;
    //Declaring the double, the sales tax charged on pants
    double salesTaxChargedOnPants;
    
    
    //Declaring the double, the total cost of purchases (before tax)
    double totalCostOfPurchases;
    //Declaring the double, the total sales tax 
    double totalSalesTax;
    //Declaring the double, the total paid for this transaction, including sales tax 
    double totalPaid;
      
    //Declaring the total cost calculation for pants
    totalCostOfPants = (double) numPants * pantsPrice;  
    //rounded to two decimal places
    totalCostOfPants = Math.floor(totalCostOfPants * 100) / 100;
    //Declaring the total cost calculation for shirts
    totalCostOfShirts = (double) numShirts * shirtPrice; 
    //rounded to two decimal places
    totalCostOfShirts = Math.floor(totalCostOfShirts * 100) / 100;
    //Declaring the total cost calculation for belts
    totalCostOfBelts = (double) numBelts * beltCost;
    //rounded to two decimals
    totalCostOfBelts = Math.floor(totalCostOfBelts * 100) / 100;
    
    //Declaring the sales tax calculation for belts 
    salesTaxChargedOnBelts = paSalesTax * totalCostOfBelts; 
    //rounded to two decimal places
    salesTaxChargedOnBelts = Math.floor(salesTaxChargedOnBelts * 100) / 100;
    //Declaring the sales tax calculation for pants
    salesTaxChargedOnPants = paSalesTax * totalCostOfPants; 
    //rounded to two decimal places
    salesTaxChargedOnPants = Math.floor(salesTaxChargedOnPants * 100) / 100;
    //Declaring the sales tax calculation for shirts
    salesTaxChargedOnShirts = paSalesTax * totalCostOfShirts; 
    //rounded to two decimal places
    salesTaxChargedOnShirts = Math.floor(salesTaxChargedOnShirts * 100) / 100;
    
    
    totalCostOfPurchases = totalCostOfShirts + totalCostOfPants + totalCostOfBelts; //Declaring the total cost of purchases by adding the costs of shirts, pants, and belts
    //rounded to two decimal places
    totalCostOfPurchases = Math.floor(totalCostOfPurchases * 100) / 100;
    
    totalSalesTax = salesTaxChargedOnShirts + salesTaxChargedOnPants + salesTaxChargedOnBelts; //Declaring the total sales tax by adding all of the sales tax from belts, shirts, and pants
    //rounded to two decimal places
    totalSalesTax = Math.floor(totalSalesTax * 100) / 100;
    
    totalPaid = totalCostOfPurchases + totalSalesTax; //Declaring the total amount paid for the transaction by adding the purchases and sales tax
    
    System.out.println(totalCostOfPants);
    System.out.println(totalCostOfShirts);
    System.out.println(totalCostOfBelts);
    System.out.println(salesTaxChargedOnShirts);
    System.out.println(salesTaxChargedOnPants);
    System.out.println(salesTaxChargedOnBelts);
    System.out.println(totalCostOfPurchases);
    System.out.println(totalSalesTax);
    System.out.println(totalPaid);
    
    
  }//end of main method
  
}