///Kevin Andrefsky
//CSE 002: Section 210, Convert Hw 03
//September 25th, 2018
//This program generates random numbers, along with selection statements, operators, and String maipulation 

import java.util.Scanner;//importing scanner
public class CrapsSwitch //defining the class
{
 
  //main method required for every Java program
  public static void main(String[] args) 
  {
    
 int dieNum1; //declaring the integers 
 int dieNum2;
    
 Scanner myScanner = new Scanner ( System.in );
 
    
 System.out.println("Would you like to randomly cast dice? (Answer Yes or No)"); //Printing the option to randomly cast the dice 
 String answer = myScanner.next(); 
   
 if  (answer.equals("No")) {  //if statement if the answer is no
   System.out.println("Enter your first number: "); 
   dieNum1 = myScanner.nextInt();
   System.out.println("Enter your second number: "); 
   dieNum2 = myScanner.nextInt();
 //Cases that print the output of the results 
   switch (dieNum1){
  case 1: 
    switch (dieNum2){
      case 1:
        System.out.println("Snake Eyes");
        break;
      case 2:
        System.out.println("Ace Deuce");
         break;
      case 3:
        System.out.println("Easy Four");
         break; 
      case 4:
        System.out.println("Fever Five");
          break;
      case 5:
        System.out.println("Easy Six");
        break;
      case 6:
        System.out.println("Seven out");
          break;
    }
       break;
       //Similar for all the cases 
  case 2:
    switch (dieNum2){
      case 1:
        System.out.println("Ace Deuce");
        break;
      case 2:
        System.out.println("Hard Four");
        break;
      case 3:
        System.out.println("Fever Five");
        break;
      case 4:
         System.out.println("Easy Six");
        break;
      case 5:
          System.out.println("Seven Out");
        break;
      case 6:
        System.out.println("Easy Eight");
        break; 
    
    }
       break;
  case 3: 
    switch (dieNum2){ 
      case 1:
        System.out.println("Easy Four");
        break;
      case 2:
        System.out.println("Fever Five");
        break;
      case 3:
        System.out.println("Hard Six");
      break;
      case 4:
        System.out.println("Seven Out");
        break;
      case 5: 
        System.out.println("Easy Eight");
        break;
      case 6:
        System.out.println("Nine");
        break;
     
   }
       break;
      
  case 4:
    switch (dieNum2){
      case 1:
        System.out.println("Fever Five");
        break;
      case 2:
        System.out.println("Easy Six");
        break;
      case 3:
        System.out.println("Seven Out");
        break;
      case 4:
        System.out.println("Hard Eight");
      break;
      case 5:
        System.out.println("Nine");
      break;
      case 6:
        System.out.println("Easy Ten");
        break;
       
    }
       break;
  case 5:
    switch (dieNum2){
      case 1:
        System.out.println("Easy Six");
        break;
      case 2:
        System.out.println("Seven Out");
        break;
      case 3:
        System.out.println("Easy Eight");
        break;
      case 4:
        System.out.println("Nine");
        break;
      case 5:
        System.out.println("Hard Ten");
        break;
      case 6:
        System.out.println("Yo-leven");
        break;
        
    }   
       break;
  case 6:
    switch (dieNum2){
      case 1:
        System.out.println("Seven Out");
        break;
      case 2:
        System.out.println("Easy Eight");
        break;
      case 3:
        System.out.println("Nine");
        break;
      case 4:
        System.out.println("Easy Ten");
        break;
      case 5:
        System.out.println("Yo-leven");
        break;
      case 6:
        System.out.println("Box Cars");
        break;
        
    }
       break;
} //end of no
 } 
   if (answer.equals("Yes")) { //If statement if the answer is yes for randomly generated number  
   dieNum1 = (int) (Math.random() * 6) + 1;
   System.out.println("Die number one is equal to: " + dieNum1);
   dieNum2 = (int) (Math.random() * 6) + 1;
   System.out.println("Die number two is equal to: " + dieNum2);
   //cases with the output depending on the reuslts
    switch (dieNum1){
  case 1:
    switch (dieNum2){
      case 1:
        System.out.println("Snake Eyes");
          break;
      case 2:
        System.out.println("Ace Deuce");
          break;
      case 3:
        System.out.println("Easy Four");
          break;
      case 4:
        System.out.println("Fever Five");
          break;
      case 5:
        System.out.println("Easy Six");
          break;
      case 6:
        System.out.println("Seven out");
          break; 
    }
        break;
        //Similar for all the cases 
  case 2:
    switch (dieNum2){
      case 1:
        System.out.println("Ace Deuce");
        break;
      case 2:
        System.out.println("Hard Four");
        break;
      case 3:
        System.out.println("Fever Five");
        break;
      case 4:
         System.out.println("Easy Six");
        break;
      case 5:
          System.out.println("Seven Out");
        break;
      case 6:
        System.out.println("Easy Eight");
        break;
    }
        break;
  case 3:
    switch (dieNum2){
      case 1:
        System.out.println("Easy Four");
        break;
      case 2:
        System.out.println("Fever Five");
        break;
      case 3:
        System.out.println("Hard Six");
        break;
      case 4:
        System.out.println("Seven Out");
        break;
      case 5: 
        System.out.println("Easy Eight");
        break;
      case 6:
        System.out.println("Nine");
        break;
        
   }
        break;
  case 4:
    switch (dieNum2){
      case 1:
        System.out.println("Fever Five");
        break;
      case 2:
        System.out.println("Easy Six");
        break;
      case 3:
        System.out.println("Seven Out");
        break;
      case 4:
        System.out.println("Hard Eight");
      break;
      case 5:
        System.out.println("Nine");
      case 6:
        System.out.println("Easy Ten");
        break;
   
    }
        break;
  case 5:
    switch (dieNum2){
      case 1:
        System.out.println("Easy Six");
        break;
      case 2:
        System.out.println("Seven Out");
        break;
      case 3:
        System.out.println("Easy Eight");
        break;
      case 4:
        System.out.println("Nine");
        break;
      case 5:
        System.out.println("Hard Ten");
        break;
      case 6:
        System.out.println("Yo-leven");
        break;
       
    }   
        break;
  case 6:
    switch (dieNum2){
      case 1:
        System.out.println("Seven Out");
        break;
      case 2:
        System.out.println("Easy Eight");
        break;
      case 3:
        System.out.println("Nine");
        break;
      case 4:
        System.out.println("Easy Ten");
        break;
      case 5:
        System.out.println("Yo-leven");
        break;
      case 6:
        System.out.println("Box Cars");
        break;
        
    } 
        break;
}
    
    }
}//end the public class and main method
}