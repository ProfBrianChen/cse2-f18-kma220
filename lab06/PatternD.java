///Kevin Andrefsky
//CSE 002: Section 210, Inputs, lab 06
//October 10th, 2018
//The purpose of this lab is to teach you nested loops and patterns that will help you understand how to set up nested loops
import java.util.Scanner; //Importing the scanner 
public class PatternD {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in);
     int length = 0;
    String junk = "";
    do{
      System.out.println("Please enter an integer between 1-10: ");
      while (!myScanner.hasNextInt()){
        System.out.println("Error, please enter an integer: ");
        junk = myScanner.nextLine();
      }
      length = myScanner.nextInt();
    }
    while (length > 10 || length < 1);
    

    for (int numRows = length; numRows >= 1; numRows--){
      for (int numCol = numRows; numCol >= 1; --numCol){
        System.out.print(numCol + " ");
      }
System.out.println();
    }
    
  }
  
}
