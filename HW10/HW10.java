import java.util.Scanner;


public class HW10 {

  public static Scanner input;
    
   
  public static void main(String[] args) {
    input = new Scanner(System.in);
    char player1 = 'o' ; //initializing and declaring the characters
    char player2 = 'x';
    boolean completeGame = false;
    char[][] board = {
      {'1','2','3'},
      {'4','5','6'},
      {'7','8','9'},
    };
    int row = 0, column = 0;
        
    printBoard(board);
    System.out.println("Player1 you are letter o and Player2 you are letter x");
    
     
       while (completeGame == false) { //loop for the tic tac toe game
          PlayerTurn(player1, board); //Player one's turn
          if (checkPlayer1Win(board, player1) == true) { //Check who won 
              completeGame = true;
              System.out.println("Player 1 wins");
              break;
          }
          PlayerTurn(player2, board); //player two's turn
          if (checkPlayer2Win(board, player2) == true) { // check if player 2 won 
              completeGame = true;
              System.out.println("Player 2 wins!");
              break;
          }
        
          if(checkDraw(player1, player2, board) == true){ //check for draw 
              System.out.println("It is a tie!");
              break;
          }
      }
  }


    
    public static void PlayerTurn(char player, char[][] board) {
        
        char player1 = 'o' ;
        char player2 = 'x';
        
      
      if(player == player1) {
            System.out.println("Enter the position: ");
            int row = input.nextInt();
            int col = input.nextInt();

    
            while ((row >= 3) || (col >= 3)) { //is it in bounds 
                System.out.println("Out of bounds index, re-enter a position");
                row = input.nextInt();
                col = input.nextInt();
            }

          
            while ((board[row][col] == player1) || (board[row][col] == player2)) { //is there an x or o in a position
                System.out.println("Already something in that position, enter another.");
                row = input.nextInt();
                col = input.nextInt();
            }
            board[row][col] = player1;
            printBoard(board);
        }
        
        else if(player == player2) {
            System.out.println("Enter the position: ");
            int row = input.nextInt();
            int col = input.nextInt();

            
            while ((row >= 3) || (col >= 3)) { //is it in bounds 
                System.out.println("Out of bounds index, re-enter a position");
                row = input.nextInt();
                col = input.nextInt();
            }

          
            while ((board[row][col] == player1) || (board[row][col] == player2)) { //x or o in the position 
                System.out.println("Already something in that position, enter another.");
                row = input.nextInt();
                col = input.nextInt();
            }
            board[row][col] = player2;
            printBoard(board);

        }
    }
  
    public static boolean checkPlayer1Win(char[][] board, char player1) { //p1 victory 
       
        for (int i = 0; i < board.length; i++) {
            if ((board[i][0] == player1) && (board[i][1] == player1) && (board[i][2] == player1)) {
                return true;
            }
        }
       
        for (int j = 0; j < board.length; j++) {
            if ((board[0][j] == player1) && (board[1][j] == player1) && (board[2][j] == player1)) {
                return true;
            }
        }
        
        if ((board[0][0] == player1) && (board[1][1] == player1) && (board[2][2] == player1)) { //diagonal victory 
            return true;
        }
        
        if ((board[0][2] == player1) && (board[1][1] == player1) && (board[2][0] == player1)) { //R to L victory 
            return true;
        } 
      else {
            return false;
        }
    }

    public static boolean checkPlayer2Win(char[][] board, char player2) {
        
        for (int i = 0; i < board.length; i++) { //same as player 1 
            if ((board[i][0] == player2) && (board[i][1] == player2) && (board[i][2] == player2)) {
                return true;
            }
        }
        
        for (int j = 0; j < board.length; j++) {
            if ((board[0][j] == player2) && (board[1][j] == player2) && (board[2][j] == player2)) {
                return true;
            }
        }
        
        if ((board[0][0] == player2) && (board[1][1] == player2) && (board[2][2] == player2)) {
            return true;
        }
        
        if ((board[0][2] == player2) && (board[1][1] == player2) && (board[2][0] == player2)) {
            return true;
        } 
      else {
            return false;
        }
    }
    public static boolean checkDraw(char player1, char player2, char[][] board){
     int count = 0;
     for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++){
                    if(board[i][j] == 'x' || board[i][j] == 'o' ){
                    count++;
                        if(count == 9){
                        return true;
           }
        }
      }
    }
        return false;
  }

    
    public static void printBoard(char[][] board) { //this method prints the board 
      int row = board.length;
      int col = board[0].length;
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }
}