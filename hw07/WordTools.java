///Kevin Andrefsky
//CSE 002: Section 210, WordTools, hw07
//October 29th, 2018
//
import java.util.Scanner; // importing the scanner
public class WordTools{
  Scanner scanner = new Scanner (System.in);  
  
   public static String shortenSpace(String sampleString){ //method to shorten the amount of white spaces 
     String shortSpace = sampleString.trim().replaceAll(" +", " "); //replace 2 spaces with one
     return shortSpace;
   }
  
  
  
     public static String replaceExclamation(String sampleString){ //method to replace !
       String replace = sampleString.replaceAll("!", "."); //replace ! with .
       return replace;
     }
  
  
  
  public static int findText(String sampleString,String word){ //Method for the number fo instances of a word in text 
    int count = 0;
    int a = 0;
    while((a = sampleString.indexOf(word)) != -1){
      sampleString = sampleString.substring(a + word.length());
      count += 1; //adding one every iteration
    }
      return count;
  }
  
  
  
  public static int getNumOfWords(String sampleString){ //method to count the number of words in a sample string 
    int count = 0;
    int totCount = 0;
    for (int a = 0; a<sampleString.length(); a++){ //loop for how big the word is
      if (sampleString.charAt(a) == ' '){ //count spaces as words
        count = count + 1;
        totCount = count + 1;
      }
    }
    totCount = count + 1;
    return totCount; //total number of words
  }
  
  
  
  public static int getNumOfNonWSCharacters(String sampleString){ //method to count the number of white spaces in a given sample string 
    int count = 0;
    for(int a = 0; a<sampleString.length(); a++){ //loop for how big the word is
      if (sampleString.charAt(a) != ' '){ //loop if there is no white space
        count = count + 1;
      }
    }
    return count;
  }
  
  
  public static void printMenu(){ //method to print the menu 
    System.out.println("Menu:");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Choose an option: ");
  }
  
  
  public static void main(String[] args){ //main method 
    Scanner scanner = new Scanner (System.in);
    
    while(true){
      System.out.println("Enter a sample text: ");
      String sampleString = scanner.nextLine();
      System.out.println("You entered: " + sampleString);
      printMenu(); //prints out the menu 
      char input = scanner.nextLine().charAt(0);
      
      switch (input){ //switches the input to the different cases and methods
        case 'q':
          System.exit(0);
        case 'c': //counts number of white spaces
          System.out.println("Number of non-whitespace characters is: " + getNumOfNonWSCharacters(sampleString));
          break;
        case 'w': //finds number of words
          System.out.println("Number of words: " + getNumOfWords(sampleString));
          break; 
        case 'f': //finds the text
          System.out.println("Enter a word or phrase to be found: ");
          String word = scanner.nextLine();
          System.out.println( word + " instances: " + findText(sampleString,word));
          break;
        case 'r': //replaces the !
          System.out.println("Edited text: " + replaceExclamation(sampleString));
          break; //prints out the fixed text 
        case 's': //shortens the spaces
          System.out.println("Edited text: " + shortenSpace(sampleString));
          break; //prints out the fixed text
        default: 
          System.out.println("Error, please try again");
      }
      System.out.println(); 
    }
  }
  }
