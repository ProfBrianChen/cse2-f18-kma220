///Kevin Andrefsky
//CSE 002: Section 210, Card Generator lab04
//September 19th, 2018
//This lab session is an exercise in using if statements, switch statements  and in using Math.random(), a random number generator.  //Importing the random math generator 
  
public class CardGenerator{
  //Main Method Begins
  public static void main (String args []){
    String suit = ""; //Declaring integers and strings
    int cardNum;
    int cardNum1;
    cardNum = (int) (Math.random() * 51) + 1; //declaring the random number generator
    cardNum1 = cardNum;
    System.out.println(cardNum + "");
    
    
    if ((cardNum>=1) && (cardNum<=13)) { //If statements printing the suit for the card number  
      suit = "Diamonds";
    }
    if ((cardNum>=14) && (cardNum<=26)) { //If statements printing the suit for the card number 
      suit = "Clubs";
      cardNum1 = cardNum - 13;
    }
    if ((cardNum>=27) && (cardNum<=39)) { //If statements printing the suit adjusted for the card number  
      suit = "Hearts";
      cardNum1 = cardNum - 26; 
    }
    if ((cardNum>=40) && (cardNum<=52)) { //If statements printing the suit adjusted for the card number 
      suit = "Spades";
      cardNum1 = cardNum - 39;
    }
    
    String cardNum2 = ""; //Switch statement assigns case number to card number  
    switch (cardNum1) {
        case 1:
        cardNum2 = "Ace"; //Similar for all cases  
        break;
        
        case 2:
        cardNum2 = "2";
        break;
        
        case 3:
        cardNum2 = "3";
        break;
        
        case 4:
        cardNum2 = "4";
        break;
        
        case 5:
        cardNum2 = "5";
        break;
        
        case 6: 
        cardNum2 = "6";
        break;
        
        case 7:
        cardNum2 = "7";
        break;
        
        case 8: 
        cardNum2 = "8";  
        break;
        
        case 9:
        cardNum2 = "9";
        break;
        
      case 10:
        cardNum2 = "10";
        break;
        
      case 11:
        cardNum2 = "Jack"; //printing the face cards based on their numbers 11-13 
        break;
        
      case 12:
        cardNum2 = "Queen";
        break;
        
      case 13:
        cardNum2 = "King";
        break;
        
    }
    
    System.out.println("You picked the " + cardNum2 + " of " + suit); //printing the result of the cardnumber and suits 
    
    
    
    
  } //end main method 
  
}  
