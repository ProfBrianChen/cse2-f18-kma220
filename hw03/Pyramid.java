///Kevin Andrefsky
//CSE 002: Section 210, Convert Hw 03
//September 18th, 2018
//The objective of this program is to input data and perform arithmetic operations with those inputs 
//The scanner must be imported to be of use in the program

import java.util.Scanner;
//main method starts

public class Pyramid {
  
  public static void main(String arg []) {
    
    //Constructing the insatance of scanner that is being declared (myScanner) 
    //Importing the scanner, the scanner will creates an instance where the scanner with take input from STDIN
    
    Scanner myScanner = new Scanner (System.in);
    
    //Declaring a double, the length of the square side of the pyramid
    int squareSide;
    //Declaring an integer, the height of the pyramid 
    int height;
    //Printing the square side of the pyramid with a space for the input 
    System.out.println("The square side of the pyramid is (input length): ");
    //Receiving the input of the square side of the pyramid 
    squareSide = myScanner.nextInt ();
    //Printing the height of the pyramid with a space for the input 
    System.out.println("The height of the pyramid is (input height): ");
    //Receiving the height of the pyramid 
    height = myScanner.nextInt ();
    //Delcaring an integer, volume  
    int volume;
    //Writing the calculation for finding the volume inside the pyramid by squaring the square side, multiplying it by the height of the pyramid and dividing by 3
    volume = (((int)(Math.pow(squareSide, 2)) * height)/3);
    //Printing the volume inside the pyramid with a "."
    System.out.println("The volume inside the pyramid is: " + volume + "." );
                  
         } //end of main method
  
    }
            
               
              