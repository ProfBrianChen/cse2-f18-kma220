///Kevin Andrefsky
//CSE 002: Section 210, Convert Hw 03
//September 18th, 2018
//The objective of this program is to input data and perform arithmetic operations with those inputs 

import java.util.Scanner;
//main method starts

public class Convert {
  
  public static void main(String arg []) {
    //Constructing the insatance of scanner that is being declared (myScanner) 
    //Importing the scanner, the scanner will creates an instance where the scanner with take input from STDIN
    
    Scanner myScanner = new Scanner (System.in);
    //Declaring the double, acres
    double acres;
    //Printing the affected area in acres with a apce for the input  
    System.out.println("Enter the affected area in acres: ");
    //Receiving the area affected in acres 
    acres = myScanner.nextDouble ();
    //Declaring the double, rainfall
    double rainfall;
    //Printing the rainfall in the affected area with a space for the input
    System.out.println("Enter the rainfall in the affected area: ");
    //Receiving the rainfall 
    rainfall = myScanner.nextDouble ();
    //Declaring the double, and writing the calculation for the amount of acreInches by multplying the acres by the rainfall
    double acreInch = acres * rainfall;
    //Declaring the double, and writing the calculation for the amount gallong of rainfall by multplying acreInches and the conversion rate 
    double gallons = acreInch * 27154.2857;
    //Declaring the double, and writing the calculation for the amount of cubic miles by multiplying square miles and the amount of rainfall
    double rainfallCubicMiles = gallons * 9.08169e-13;
    //Printing the amount of cubic miles of rainfall with a label of "cubic miles."
    System.out.println(rainfallCubicMiles + " cubic miles.");
    
    
    
  } //end of main method
  
} 